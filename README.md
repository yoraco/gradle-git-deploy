# gradle-git-deploy
Deployment of gradle based libs on Git repositories


## Usage

### 1. Global gradle.properties
Setup your global gradle settings in your home gradle.properties file (usually located in ~/.gradle/).

```properties
REPOSITORY_USERNAME=username
REPOSITORY_PASSWORD=passwd
```

### 2. Project & module level gradle.properties
You can setup (maybe it's already done) your properties at a project and module level. You may for example provide a sample module which you don't want to deploy because it makes no sense, but some of your properties are "project level". An example could be like this : 

Project : 
```properties
POM_DESCRIPTION=Meaningfull description of your artifact
POM_NAME=YourArtifactReadableName
POM_ARTIFACT_GROUP=de.yourgroup.id
POM_ARTIFACT_ID=your-artifact-id
POM_ARTIFACT_VERSION= 1.0
VERSION_CODE=1

POM_URL=Lib Url
POM_SCM_URL=Scm Url
POM_SCM_CONNECTION=...
POM_SCM_DEV_CONNECTION=...
POM_LICENCE_NAME=...
POM_LICENCE_URL=...
POM_LICENCE_DIST=repo
POM_DEVELOPER_ID=you
POM_DEVELOPER_NAME=You
POM_DEVELOPER_EMAIL=You@...
```

& library module :
```properties
POM_NAME=Your Great Lib
POM_ARTIFACT_ID=lib_artifact_id
POM_PACKAGING=aar(or jar)
```

& repository url:
```properties
REPOSITORY_RELEASE_URL "git:releases://git@bitbucket.org:<bitbucket-username>/<your-repo>.git"
REPOSITORY_SNAPSHOT_URL "git:snapshots://git@bitbucket.org:<bitbucket-username>/<your-repo>.git"
```

### 3. Deploy module

Make sure that the repository you want to push in contains at minimun a README.md file otherwise wagon can't pull the repository.

Then, you're just 2 steps closer to deploy your module, the first one is to update your gradle build file to add the dependency on the deploy file:

Reference as a remote url
```groovy
apply from: 'https://bitbucket.org/yoraco/gradle-git-deploy/raw/HEAD/deploy.gradle'
```
or from a local
```groovy
apply from: './deploy.gradle'
```

And now, build (& clean, if necessary), & upload to your repo:

```bash
$ gradle [clean build] uploadArchives
```

### 4. Get your dependencies

```bash
allprojects {
    repositories {
        mavenCentral()
        maven {
            url "https://api.bitbucket.org/2.0/repositories/<bitbucket_username>/<bitbucket-repository>/src/snapshots"
            credentials {
                username REPOSITORY_USERNAME
                password REPOSITORY_PASSWORD
            }
            authentication {
                basic(BasicAuthentication)
            }
        }
        maven {
            url "https://api.bitbucket.org/2.0/repositories/<bitbucket_username>/<bitbucket-repository>/src/releases"
            credentials {
                username REPOSITORY_USERNAME
                password REPOSITORY_PASSWORD
            }
            authentication {
                basic(BasicAuthentication)
            }
        }
    }
}
```

## Troubleshooting


Your ~/.ssh/known_hosts file is missing the fingerprint of the bitbucket server when see this error:
```bash
RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
Are[INFO] [git] Host key verification failed.
[INFO] [git] fatal: Could not read from remote repository.
[INFO] [git] 
[INFO] [git] Please make sure you have the correct access rights
[INFO] [git] and the repository exists.
:numbertweeiningview:uploadArchives (Thread[Task worker Thread 4,5,main]) completed. Took 4.903 secs.

```
you can add bitbucket.org to your known_host file

```bash
ssh -o StrictHostKeyChecking=no yoraco@bitbucket.org
```

you can get some extra help from BitBucket at https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html#SetupSSHforGit-startagent
	
## License

    Copyright 2016

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
